# Python Script For Comparing Data

This script allows you to compare results of the DSSP v.4 (or DSSP v.2) algorithm and DSSP algorithm implemented in Gromacs by comparing the data (secondary structure assignments) in the output files.

**DSSP ver. ≥ 4.0.0 OR =2.2.1** and **standalone version of DSSP algorithm implementation in Gromacs** (https://gitlab.com/bio-pnpi/gmx-dssp) are required for the script to work. You **_must_** also set the "**MKDSSP**" and "**GMXDSSP**" environment variables to DSSP v.4/v.2 and the Gromacs implementation executables (respectively).

```
export MKDSSP=/usr/bin/mkdssp
export GMXDSSP=\<gmx-dssp\>
```

For verbose output, use the -v input option.

You can also compare arbitrary PDB files, but be aware that *some* PDB files may have strange internal contents and thus _somethimes_ cannot be checked (either MKDSSP will not accept such a file as input, or Gromacs will read it with errors and, therefore, will create an output file that differs in content from DSSP v.4 or DSSP v.2).
