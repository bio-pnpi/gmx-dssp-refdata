#!/usr/bin/env python3

import argparse
import difflib
import os
from packaging import version
import re
import subprocess

mkdssp_desired_version = '4.0.0'
mkdssp_possible_version = '2.2.1'
using_old_mkdssp: bool = False
gmx_desired_version = '2023.1'

parser = argparse.ArgumentParser(description='Python script to compare output from "gmx dssp" and "mkdssp". Before running this script, make sure that you set the GMXDSSP environment variable to the GROMACS executable binary and the MKDSSP environment variable to the mkdssp executable binary!')
parser.add_argument('-v', '--verbose', type=bool, default=False, help='Verbose mode', action=argparse.BooleanOptionalAction)
args = parser.parse_args()

trigger_gmx_exception: bool = False
trigger_mkdssp_exception: bool = False
var_gmx: str
var_mkdssp: str

if not (var_gmx := os.environ.get('GMXDSSP')):
    trigger_gmx_exception = True

if not (var_mkdssp := os.environ.get('MKDSSP')):
    trigger_mkdssp_exception = True

if trigger_gmx_exception and trigger_mkdssp_exception:
    raise ValueError(f'GMXDSSP and MKDSSP environmental variables not set!')
elif trigger_gmx_exception:
    raise ValueError(f'GMXDSSP environmental variable not set!')
elif trigger_mkdssp_exception:
    raise ValueError(f'MKDSSP environmental variable not set!')
elif args.verbose:
    print(f'GMXDSSP and MKDSSP environmental variables detected.')

fail_counter: int = 0
pattern_base: re.compile = re.compile('^\s*\d+\s*-?\d+\s*[a-zA-Z]+\s*[a-zA-Z]\s+((?:\s|[a-zA-Z]))')
pattern_breaks: re.compile = re.compile('^\s*\d+\s*(!)')
mkdssp_process: subprocess = subprocess.run('$MKDSSP --version', shell=True, capture_output=True, text=True)
version_patterm_mkdssp: re.compile = re.compile('\d+.\d+.\d+')
version_mkdssp: str = version_patterm_mkdssp.findall(mkdssp_process.stdout)[0]
if args.verbose:
    print(f'Detected mkdssp version {version_mkdssp}.')
if version.parse(version_mkdssp) == version.parse(mkdssp_possible_version):
    using_old_mkdssp = True
elif version.parse(version_mkdssp) < version.parse(mkdssp_desired_version):
    raise ImportError(f'Mkdssp version ({version_mkdssp}) is lower than required ({mkdssp_desired_version})!')

gmxdssp_process: subprocess = subprocess.run('$GMXDSSP --version', shell=True, capture_output=True, text=True)
version_patterm_gmx: re.compile = re.compile('version:\s+(.+)\s+')
version_gmx: str = version_patterm_gmx.findall(gmxdssp_process.stdout)[0]
if args.verbose:
    print(f'Detected gmx version {version_gmx}.')
if version.parse(version_gmx) < version.parse(gmx_desired_version):
    raise ImportError(f'Gmx version ({version_gmx}) is lower than required ({gmx_desired_version})!')

pdbs_process: subprocess = subprocess.run('ls proteins/*.pdb', shell=True, capture_output=True, text=True)
pdbs: list['str'] = pdbs_process.stdout.replace('proteins/','').split('\n')
pdbs.remove('')

mkdssp_gfs_process: subprocess = subprocess.run('ls mkdssp-output/*.mkdssp', shell=True, capture_output=True, text=True)
mkdssp_gfs: list['str'] = mkdssp_gfs_process.stdout.replace('mkdssp-output/','').split('\n')
mkdssp_gfs.remove('')

gmxdssp_gfs_process: subprocess = subprocess.run('ls gmxdssp-output/*.dat', shell=True, capture_output=True, text=True)
gmxdssp_gfs: list['str'] = gmxdssp_gfs_process.stdout.replace('gmxdssp-output/','').split('\n')
gmxdssp_gfs.remove('')

if args.verbose:
    print(f'{len(pdbs)} PDB file(s) will be analysed: {str(pdbs).translate({ord(i): None for i in "[]"})}.')
for i in pdbs:
    print(f'Analysing {i}...')
    if i.replace('.pdb', '.mkdssp') not in mkdssp_gfs:
        if args.verbose:
            print(f'Generating output file via mkdssp for {i}...')
        if using_old_mkdssp:
            subprocess.run(f'$MKDSSP -i proteins/{i} -o mkdssp-output/{i.replace(".pdb",".mkdssp")}', shell=True, capture_output=True, text=True)
        else:
            subprocess.run(f'$MKDSSP --output-format=dssp proteins/{i} mkdssp-output/{i.replace(".pdb",".mkdssp")}', shell=True, capture_output=True, text=True)
    elif args.verbose:
        print(f'Using existing {i.replace(".pdb", ".mkdssp")} file in mkdssp-output folder.')

    if i.replace('.pdb', '.dat') not in gmxdssp_gfs:
        if args.verbose:
            print(f'Generating output file via gromacs for {i}...')
        if using_old_mkdssp:
            subprocess.run(f'$GMXDSSP -f proteins/{i} -s proteins/{i} -clear -pihelix -nopolypro -hmode dssp -o gmxdssp-output/{i.replace(".pdb","")}', shell=True, capture_output=True, text=True)
        else:
            subprocess.run(f'$GMXDSSP -f proteins/{i} -s proteins/{i} -clear -pihelix -hmode dssp -o gmxdssp-output/{i.replace(".pdb","")}', shell=True, capture_output=True, text=True)
    elif args.verbose:
        print(f'Using existing {i.replace(".pdb", ".dat")} file in gmxdssp-output folder.')

    with open('mkdssp-output/' + i.replace('.pdb','.mkdssp'), 'r') as mkdssp_file:
        assignment_mkdssp: str = ''
        for line in mkdssp_file:
            if re.search('^\s*\d+\s*-?\d+\s*[a-zA-Z]+\s*[a-zA-Z]\s+(?:\s|[a-zA-Z])', line):
                assignment_mkdssp += pattern_base.findall(line)[0]
            elif re.search('^\s*\d+\s*!', line):
                assignment_mkdssp += pattern_breaks.findall(line)[0]
        assignment_mkdssp = assignment_mkdssp.replace('X', ' ').replace(' ', '~').replace('!', '=')
        if args.verbose:
            print(f'{i} MKDSSP\t\t:', assignment_mkdssp)

    with open('gmxdssp-output/' + i.replace('.pdb','.dat'), 'r') as gmx_file:
        assignment_gmx: str = ''
        for line in gmx_file:
            assignment_gmx = line.replace('\n', '')
        if args.verbose:
            print(f'{i} GMXDSSP\t:', assignment_gmx)

    if assignment_mkdssp == assignment_gmx:
        print(f'{i} — Success!')
    else:
        fail_counter += 1
        print(f'{i} — Failure!')

print(f'RESULT: Failed: {fail_counter}, Succeeded: {len(pdbs) - fail_counter}.')
